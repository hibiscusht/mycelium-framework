<?php
namespace Eloquents;

use Eloquents\Eloquent;

class Users extends Eloquent
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function setSchema($table,$primary)
    {
        self::schema(['table_name'=>$table,'primary_key'=>$primary]);
    }
}