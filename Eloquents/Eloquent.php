<?php
namespace Eloquents;

use Library\Env; 
use System\Eloquent\Eloquent as DB;

class Eloquent extends DB
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function initialise()
    {
        self::init([
            'connection'=>[
                'hostname'=>Env::load('DB_HOST'),
                'user'=>Env::load('DB_USER'),
                'pass'=>Env::load('DB_PASS'),
                'database'=>Env::load('DB_NAME')
            ],
            'dialect'=>'mysql'
        ]);
    }

}