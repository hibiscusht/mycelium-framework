<?php
namespace System\Eloquent;
 
class Eloquent
{
    private static $method;
    private static $host;
    private static $user;
    private static $pass;
    private static $database;
    private static $schema;
    private static $primary;


    public function __construct()
    {
        
    }

    protected static function schema($opt)
    {
        self::$schema = $opt['table_name'];
        self::$primary = $opt['primary_key'];
    }

    protected static function init($options)
    {
            self::$method = $options['dialect'];
            self::$host = $options['connection']['hostname'];
            self::$user = $options['connection']['user'];
            self::$pass = $options['connection']['pass'];
            self::$database = $options['connection']['database'];
    }

    private function mysql($sql)
    {
         $conn = mysqli_connect(self::$host,self::$user,self::$pass,self::$database);
         $res = mysqli_query($conn,$sql);
         return mysqli_fetch_assoc($res);
    }
    
    public static function findAll($opt)
    {
         $methods = get_class_methods(__CLASS__);
         $return = '';
         foreach($methods as $meth){
                if($meth == self::$method && $meth == 'mysql'){
                    $return = self::mysql('SELECT * FROM ' . self::$schema);
                    break;
                }
         }
         return $return;
    }

}