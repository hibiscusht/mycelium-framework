<?php
namespace System\Router;

class Router
{
    private static $controller;
    private static $namespace;
    private static $status;
    private static $check = false;
    private static $request = [];
    private static $array = [];

    private static function checkUri($uri)
    {
        require 'config/config.php';
        $base_url = $config['base_url'];
        $base = str_replace('http://' . $_SERVER['HTTP_HOST'],'',$base_url);
        $url = str_replace($base,'',$_SERVER['REQUEST_URI']);
        $qs = str_replace('?' . $_SERVER['QUERY_STRING'],'',$url);
        if($uri == $_SERVER['REQUEST_URI']){
            return true;
        } else if($uri == $url){
            return true;
        } else if($uri == $qs){
            return true;
        } else {
           return false;
        } 
    }
    public static function get(...$args)
    {
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
             if(self::checkUri($args[0]) && !self::$check){
                   $controller = '';
                if(is_string($args[1])){
                    $controller = explode('::',$args[1]);
                } else {
                    // middleware ?
                    $middle = $args[1];
                    self::$request[$middle[0]] = $middle[1];
                    $controller = explode('::',$args[2]);
                }
                    $namespace = str_replace('\\','/',$controller[0]);
                    if(file_exists($namespace . '.php')){
                        self::$controller = $controller;
                        self::$namespace = $namespace;
                        self::$status = true; 
                        self::$check = true;
                    } else {
                        self::$status = false;
                    }
            }  
        }
    }
    
    public static function loadController()
    {
        if(!self::$status){
            echo 'WHOOPS! WRONG CONTROLLER'; 
        } else {
            $o = (object) self::$request;
            $ns = self::$namespace; 
            $c = self::$controller;
            $obj = $c[0];
            $meth = $c[1];
            include $ns . '.php';
            $cls = new $obj();
            if(count(self::$request) > 0){
                $cls->$meth($o);  
            } else {
                $cls->$meth();  
            }
           
        }
       
    }
}