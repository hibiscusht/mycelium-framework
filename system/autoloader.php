<?php
namespace System\Autoloader;


require 'route.php';
        

use System\Router\Router;


class Autoloader
{
    
    private static function loadRoutes()
    {
        require 'config/routes.php';
        require 'controller.php';
        Router::loadController();
    }

    private static function loadLibrary()
    {
        require 'config/config.php';
        foreach($config['library'] as $conf){
            require 'library/' . $conf . '.php'; 
        }
    }

    private static function loadMiddleware()
    {
        spl_autoload_register(function($name)
            {
                $namespace = str_replace('\\','/',$name);
                require $namespace . '.php';
            }
        ,true,false);
    }

    private static function loadEloquent()
    {
        require 'eloquent.php';
        require 'Eloquents/Eloquent.php';
        spl_autoload_register(function($name)
            {
                $namespace = str_replace('\\','/',$name);
                require $namespace . '.php';
            }
        ,true,false);
    }

    public static function loader()
    {
        
        self::loadMiddleware();
        self::loadLibrary();
        self::loadEloquent();
        self::loadRoutes();
    }
}