<?php
namespace System\Controller;

class Controller
{

    public function __construct()
    {
        
    }

    public function __get($name)
    {
        if($name == 'customRequest'){
            return $this;
        }
    }

    public function get($name = null)
    {
        if(is_null($name)){
            return $_GET;
        } else {
            return $_GET[$name];
        }
    }
}