<?php
namespace Library; 

class Env
{
    private static $path;
    private static function findEnv()
    {
        self::$path = dirname(__DIR__,1);
        if(file_exists(self::$path . '/.env')){
            return self::$path . '/.env';
        } else {
            return false;
        }
    }
    public static function load($key)
    {
        $file = self::findEnv();
        if(!$file){
            return 'WHOOPS! CANNOT FIND ENV';
        } else {
            $contents = file_get_contents($file);
            $data = preg_split('/\n/',$contents);
            $env = null;
            foreach($data as $row){
                if($row != ''){
                    $rows = explode('=',$row);
                    if($rows[0] == $key){
                        $env = trim($rows[1]);
                        break;
                    }
                }
            }
            return $env;
        }
    }
}