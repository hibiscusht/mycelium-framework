<?php

use System\Router\Router;

use Middleware\Check;

Router::get('/test',"Controllers\Test::index");

Router::get('/cek',Check::tes(),"Controllers\Demo::check");

Router::get('/user',"Controllers\Demo::getUser");

Router::get('/input',"Controllers\Demo::tesInput");