<?php
namespace Controllers;

use Eloquents\Barangs;
use Eloquents\Users;
use System\Controller\Controller;

class Demo extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function check($request)
    {
        echo '<center><h1>SUCCESSSS</h></center>';
        echo '<br/>';
        echo 'nilai dari middleware ' . $request->status;
    }

    public function getUser()
    {
        Users::initialise();
        Users::setSchema('users','Id');
        var_dump(Users::findAll([]));
        echo '<hr>';
        Barangs::initialise();
        Barangs::setSchema('barangs','Id');
        var_dump(Barangs::findAll([]));
    }
    
    public function tesInput()
    {
        var_dump($this->customRequest->get());
    }
}