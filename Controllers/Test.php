<?php
namespace Controllers;

use Library\Env;

class Test
{
    public function __construct()
    {
        
    }
    public function index()
    {
        echo '<center><h1>Hello from mycelium</h1></center>';
        echo '<br/>';
        echo 'App name ' . Env::load('APP_NAME');
        echo '<br/>';
        echo 'DB Hostname ' . Env::load('DB_HOST');
       // var_dump(Env::load(''));
    }
}